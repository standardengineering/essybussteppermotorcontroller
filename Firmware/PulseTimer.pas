//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit PulseTimer;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses CPU;

{--------------------------------------------------------------}
{ Const Declarations }
const
  ENCODERPULSESPERREVOLUTION: Word = 512;
  MOTORPULSESPERREVOLUTION: Word = 1600;
  PULSESRATIO: Float = MOTORPULSESPERREVOLUTION / ENCODERPULSESPERREVOLUTION;

{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Var Declarations }

{--------------------------------------------------------------}
{ functions }
procedure InitPulseTimer;
procedure EnablePulseTimer;
procedure DisablePulseTimer;
procedure SetPulseTimerSpeed(aPulsesPerSecond: Float);


implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Const Declarations }
const
  TIMERCLOCK: Float = PROCCLOCK / 8.0; //prescaler of 8
{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var
  fTimerValue: Word;
{--------------------------------------------------------------}
{ functions }

procedure DisablePulseTimer;
begin
  TCCR1A := %00000011;
end;

procedure InitPulseTimer;
begin
  TCCR1A := %01000011;
  TCCR1B := %00011010; // prescaling : /8
  OCR1AH := 255;  //init timer at slow speed (to prevent clogop of CPU)
  OCR1AL := 255;  //init timer at slow speed (to prevent clogop of CPU)
  fTimerValue := 65535;
  DisablePulseTimer;
end;

procedure EnablePulseTimer;
begin
  TCCR1A := %01000011;
end;

procedure SetPulseTimerSpeed(aPulsesPerSecond: Float);
var
  tempTimerValue: Float;
begin
  tempTimerValue := (TIMERCLOCK / (aPulsesPerSecond * 2.0 * PULSESRATIO));
  if tempTimerValue > 65535 then
    fTimerValue := 65535;
  else
    fTimerValue := Word(Round(tempTimerValue));
  endif;
  if tempTimerValue < 2 then
    fTimerValue := 2;
  endif;
  OCR1AH := Hi(fTimerValue);
  OCR1AL := Lo(fTimerValue);
end;

initialization

end PulseTimer.

