//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit Encoder;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses CPU;

{--------------------------------------------------------------}
{ Const Declarations }
const

{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Var Declarations }
var

{--------------------------------------------------------------}
{ functions }
procedure InitEncoder;
function GetPosition: LongInt;
procedure SetPosition(aPosition: LongInt);


implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Const Declarations }
const

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var
  fPosition: LongInt;
{--------------------------------------------------------------}
{ functions }

procedure InitEncoder;
begin
  PCICR  := %00000100; //enable Pin Change Interrupt 2
  PCMSK2 := %00010000; //enable PCInt20 (Encoder DIR pin)
  fPosition :=  0; //reset position
end;

function GetPosition: LongInt;
var
  tempPos: LongInt;
begin
  DisableInts;
  tempPos := fPosition;
  EnableInts;
  return(tempPos);
end;

procedure SetPosition(aPosition: LongInt);
begin
  DisableInts;
  fPosition := aPosition;
  EnableInts;
end;

interrupt PCINT2; // Change of channel A
begin
  GreenLed := True;
  if (Enc_A xor Enc_B) then
    inc(fPosition);
  else
    dec(fPosition);
  endif;
  GreenLed := False;
end;

initialization
// at StartUp

// finalization          // optional
// at System_ShutDown
end Encoder.

