//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit PWM;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses ;

{--------------------------------------------------------------}
{ Const Declarations }
const

{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Var Declarations }
var
  SysTickHappened: Boolean;
 
{--------------------------------------------------------------}
{ functions }
procedure InitPWM;
procedure SetCurrent(aCurrent: Word);


implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Const Declarations }
const

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var
  fInterruptCounter: Byte;
{--------------------------------------------------------------}
{ functions }

procedure InitPWM;
begin
  fInterruptCounter := 10;
  SysTickHappened := False;
  TCCR2A := %10000001; //Clear OC2A on Compare Match, Phase Correct PWM
  TCCR2B := %00000011; //CLK / 32  @20 Mhz = 625 KHz
  OCR2A := 0;
  TIMSK2 := %00000001; //Timer/Counter2 Overflow Interrupt Enable (for systick emulation)
end;

procedure SetVoltage(aVoltage: Float);
begin
  if aVoltage > 4.0 then
    aVoltage := 4.0;
  endif;
  OCR2A := trunc(aVoltage / 0.01953125);
end;

procedure SetCurrent(aCurrent: Word);
begin
  SetVoltage((Float(aCurrent) / 1000.0) * 4.0);
end;

{interrupt TIMER2; //used instead of a Systick
begin
  inc(fInterruptCounter);
  if fInterruptCounter > 4 then
    fInterruptCounter := 0;
    SysTickHappened := True;
  endif;
end;}

interrupt TIMER2; //used instead of a Systick
begin
  ASM;
    LDS       _ACCA, PWM.fInterruptCounter
    DEC       _ACCA
    STS       PWM.fInterruptCounter, _ACCA
    TST       _ACCA
    BRNE      PWM.ENDTIMER2
    LDI       _ACCA, 004h
    STS       PWM.fInterruptCounter, _ACCA
    LDI       _ACCA, 00FFh
    STS       PWM.SYSTICKHAPPENED, _ACCA
    PWM.ENDTIMER2:
  ENDASM;
end;


initialization
// at StartUp

// finalization          // optional
// at System_ShutDown
end PWM.

