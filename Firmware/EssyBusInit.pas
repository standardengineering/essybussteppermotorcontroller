//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit EssyBusInit;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses;

{--------------------------------------------------------------}
{ Const Declarations }
const

{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type
  TEssyBusInterruptReason = (erNone, erDestinationReached, erStopchannel1Happened, erStopChannel2Happened, erStalled);
  
{$DEFINE EB_LEDINUSE}

{--------------------------------------------------------------}
{ Var Declarations }
var
  EB_FirmwareVersion: Array[0..3] of Byte;
  EB_ReceiveDataBuffer: Array[0..15] of Byte;
  EB_UDR[@UDR1]: Byte; //RS232 data register
  EB_SendPin[@PortC, 4]: Bit;
  EB_IntPin[@PortD, 6]: Bit;
  EB_DataLed[@PortB, 5]: Bit;
  EB_TXC[@UCSR0A, 6]: Bit;  //transmit complete
  EB_UDRE[@UCSR0A, 5]: Bit;  //Data Register Empty
  EB_RXC[@UCSR0A, 7]: Bit;  //receive complete
  EB_ERR[@UCSR0A, 4]: Bit;  //Frame Error

{--------------------------------------------------------------}
{ functions }
procedure EB_LowLvlInit;
procedure EB_WaitForTransmissionToComplete;
procedure EB_WaitForEmptyTransmitBuffer;
procedure EB_SendEOTToEssyBus;

implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Const Declarations }
const
  EOT: Byte = 253;
{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var

{--------------------------------------------------------------}
{ functions }
procedure EB_LowLvlInit;
begin
  UCSR0A := %00000000; // Normal Speed, multiprocessor disable
  UCSR0B := %10011000; //receive int enabled, transmit and receive enabled, 8-Databits
  UCSR0C := %00000110; //8-Databits
  UBRRL := 24; //50 KBaud @ 20 Mhz
  EB_SendPin := false;
  EB_DataLed := false;
  EB_IntPin := false;
end;

procedure EB_WaitForTransmissionToComplete;
begin
  repeat
  until EB_TXC;
  EB_TXC := True;
end;

procedure EB_WaitForEmptyTransmitBuffer;
begin
  repeat
  until EB_UDRE;
end;

procedure EB_SendEOTToEssyBus;
begin
  EB_TXC := True;
  EB_UDR := EOT;
  EB_WaitForTransmissionToComplete();
end;

initialization
// at StartUp
  EB_FirmwareVersion[0] := 9; //Major Version
  EB_FirmwareVersion[1] := 0; //Minor Version
  EB_FirmwareVersion[2] := 0; //Build Number
  EB_FirmwareVersion[3] := 0; //Revision Number
  
end EssyBusInit.

