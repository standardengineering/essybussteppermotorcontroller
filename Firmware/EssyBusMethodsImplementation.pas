//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit EssyBusMethodsImplementation;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses
  CPU,
  EssyBus,
  EssyBusInterrupt,
  EssyBusInit,
  Motor;
{--------------------------------------------------------------}
{ Const Declarations }
const
  EB_MAXNUMBEROFMETHODS: Byte = 25;
{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Var Declarations }
var
  EB_Methods: array[1..EB_MAXNUMBEROFMETHODS] of procedure;
{--------------------------------------------------------------}
{ functions }

implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type
  TStopChannelPullupState = (spsNoPullUps, spsChannelOnePullUp, spsChannelTwoPullUp, spsBothChannelsPullUp);
{--------------------------------------------------------------}
{ Const Declarations }
const

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var

{--------------------------------------------------------------}
{ functions }
procedure fGetInterruptReason;
var
  tempEBReason: TEssyBusInterruptReason;
begin
  tempEBReason := GetEssyBusInterruptReason();
  SendDataBufferToEssyBus(@tempEBReason, 1);
end;

procedure fSetInterruptReason;
begin
  SetEssyBusInterruptReason(TEssyBusInterruptReason(EB_ReceiveDataBuffer[0]));
end;

procedure fEnableHBridge;
begin
  StepperDisabled := False;
end;

procedure fDisableHBridge;
begin
  StepperDisabled := True;
end;

procedure fSetAccelleration;
begin
  RampUpAccel := GetInt32fromEssyBusReceiveBuffer(0);
end;

procedure fSetDecelleration;
begin
  RampDownAccel := GetInt32fromEssyBusReceiveBuffer(0);
end;

procedure fSetSpeed;
begin
  MaxSpeed := GetWordfromEssyBusReceiveBuffer(0);
end;

procedure fGetPosition;
var
  tempPosition: LongInt;
begin
  tempPosition := GetPosition();
  SendDataBufferToEssyBus(@tempPosition, 4);
end;

procedure fSetPosition;
begin
  SetPosition(GetInt32fromEssyBusReceiveBuffer(0));
end;

procedure fGetStepsPerRevolution;
var
  tempSteps: Word;
begin
  tempSteps := ENCODERPULSESPERREVOLUTION;
  SendDataBufferToEssyBus(@tempSteps, 2);
end;

procedure fSetStallDetectionEnabled;
begin
  StallDetectionEnabled := Boolean(EB_ReceiveDataBuffer[0]);
end;

procedure fSetStopChannel;
var
  tempStopChannel: TStopChannel;
begin
  case EB_ReceiveDataBuffer[0] of
    1:  CopyBlock(@EB_ReceiveDataBuffer + 1, @StopChannel1, 12);|
    2:  CopyBlock(@EB_ReceiveDataBuffer + 1, @StopChannel2, 12);|
  endcase;
end;

procedure fMoveToPosition;
begin
  StopChannel1.Enabled := False;
  StopChannel2.Enabled := False;
  DestinationPosition := GetInt32fromEssyBusReceiveBuffer(0);
  MotorTrajectoryMove();
end;

procedure fSetIdleCurrent;
begin
  IdleCurrent := GetWordfromEssyBusReceiveBuffer(0);
  if (RampStatus = rsOff) or (RampStatus = rsStalled) then
    SetCurrent(IdleCurrent);
  endif;
end;

procedure fSetMoveCurrent;
begin
  MoveCurrent := GetWordfromEssyBusReceiveBuffer(0);
end;

procedure fResetDevice;
begin
  SetEssyBusInterruptReason(erNone);
  InitMotor();
  InitEncoder();
end;

procedure fMotorConstantVelocityMoveClockWise;
begin
  StopChannel1.Enabled := False;
  StopChannel2.Enabled := False;
  MotorConstantVelocityMoveClockWise();
end;

procedure fMotorConstantVelocityMoveCCW;
begin
  StopChannel1.Enabled := False;
  StopChannel2.Enabled := False;
  MotorConstantVelocityMoveCounterClockWise();
end;

procedure fMotorMoveCWUntilStop;
begin
  StopChannel1.Enabled := Boolean(EB_ReceiveDataBuffer[0]);
  StopChannel2.Enabled := Boolean(EB_ReceiveDataBuffer[1]);
  MotorConstantVelocityMoveClockWise();
end;

procedure fMotorMoveCCWUntilStop;
begin
  StopChannel1.Enabled := Boolean(EB_ReceiveDataBuffer[0]);
  StopChannel2.Enabled := Boolean(EB_ReceiveDataBuffer[1]);
  MotorConstantVelocityMoveCounterClockWise();
end;

procedure fSetPullUps;
var
  stopChannelState: TStopChannelPullupState;
begin
  stopChannelState := TStopChannelPullupState(EB_ReceiveDataBuffer[0]);
  case stopChannelState of
    spsNoPullUps:
      Stop1PullUp := false;
      Stop2PullUp := false;|
    spsChannelOnePullUp:
      Stop1PullUp := true;
      Stop2PullUp := false;|
    spsChannelTwoPullUp:
      Stop1PullUp := false;
      Stop2PullUp := true;|
    spsBothChannelsPullUp:
      Stop1PullUp := true;
      Stop2PullUp := true;|
  endcase;
end;

procedure fGetStoredPosition;
begin
  SendDataBufferToEssyBus(@StoredPosition, 4);
end;

procedure fMoveToPositionWithCapture;
begin
  StopChannel1.Enabled := Boolean(EB_ReceiveDataBuffer[4]);
  StopChannel2.Enabled := Boolean(EB_ReceiveDataBuffer[5]);
  DestinationPosition := GetInt32fromEssyBusReceiveBuffer(0);
  MotorTrajectoryMove();
end;

procedure fSetEarlyArrivalDelta;
begin
  EarlyArrivalDelta := GetWordfromEssyBusReceiveBuffer(0);
end;

initialization
  EB_Methods[1] := @fGetInterruptReason;
  EB_Methods[2] := @fSetInterruptReason;
  EB_Methods[3] := @fEnableHBridge;
  EB_Methods[4] := @fDisableHBridge;
  EB_Methods[5] := @fSetAccelleration;
  EB_Methods[6] := @fSetDecelleration;
  EB_Methods[7] := @fSetSpeed;
  EB_Methods[8] := @fGetPosition;
  EB_Methods[9] := @fSetPosition;
  EB_Methods[10] := @fGetStepsPerRevolution;
  EB_Methods[11] := @fSetStallDetectionEnabled;
  EB_Methods[12] := @fSetStopChannel;
  EB_Methods[13] := @fMoveToPosition;
  EB_Methods[14] := @fMotorConstantVelocityMoveClockWise;
  EB_Methods[15] := @fMotorConstantVelocityMoveCCW;
  EB_Methods[16] := @MotorConstantVelocityStop;
  EB_Methods[17] := @fSetIdleCurrent;
  EB_Methods[18] := @fSetMoveCurrent;
  EB_Methods[19] := @fResetDevice;
  EB_Methods[20] := @fMotorMoveCWUntilStop;
  EB_Methods[21] := @fMotorMoveCCWUntilStop;
  EB_Methods[22] := @fSetPullUps;
  EB_Methods[23] := @fGetStoredPosition;
  EB_Methods[24] := @fMoveToPositionWithCapture;
  EB_Methods[25] := @fSetEarlyArrivalDelta;
  
end EssyBusMethodsImplementation.

