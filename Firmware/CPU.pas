//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit CPU;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses ;

{--------------------------------------------------------------}
{ Const Declarations }


{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type


{--------------------------------------------------------------}
{ Var Declarations }
var
  StepperDirection[@PortD, 7] : Bit;
  StepperPulse[@PortB, 1] : Bit;
  StepperDisabled[@PortB, 0] : Bit;
  Enc_A[@PinD, 4] : Bit;
  Enc_B[@PinD, 2] : Bit;
  Stop1[@PinD, 3] : Bit;
  Stop2[@PinD, 5] : Bit;
  Stop1PullUp[@PortD, 3] : Bit;
  Stop2PullUp[@PortD, 5] : Bit;
  RedLed[@PortB, 4] : Bit;
  GreenLed[@PortB, 2] : Bit;

{--------------------------------------------------------------}
{ functions }
procedure InitPorts;


implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type

{--------------------------------------------------------------}
{ Const Declarations }
const

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var

{--------------------------------------------------------------}
{ functions }

procedure InitPorts;
begin
  DDRB := %00111111; //all output except bit 7, 6
  DDRC := %00110000; // all input except bit 4, 5
  PortC := %00001111; //enable pull-ups on first 4 bits.(address switch)
  DDRD := %11000000;
end;

initialization
// at StartUp

// finalization          // optional
// at System_ShutDown
end CPU.

