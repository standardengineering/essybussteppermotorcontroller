//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

program EssyBusStepper;

Device = mega168, VCC=5;

Import ;

From System Import LongWord, LongInt, Float;

Define
  ProcClock      = 20000000;       {Hertz}
  StackSize      = 512, iData;
  FrameSize      = 128, iData;

uses
  CPU, PWM, PulseTimer, Encoder, EssyBus, EssyBusMethods, Motor;

Implementation

{$IDATA}

{--------------------------------------------------------------}
{ Type Declarations }

type

{--------------------------------------------------------------}
{ Const Declarations }

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}

{--------------------------------------------------------------}
{ functions }

interrupt RXRDY;
begin
  EssyBusDoReceiveData();
end;

{--------------------------------------------------------------}
{ Main Program }
{$IDATA}
var
  followerAddress: Byte;
  i: Byte;
begin
  InitPorts();
  //get follower address
  followerAddress := (not PinC) and %00001111;  //mask upper nibble and invert lower nibble
  if followerAddress = 0 then followerAddress := 10; endif;
  InitPulseTimer();
  InitPWM();
  InitMotor();
  InitEncoder();
  InitEssyBus(followerAddress, @EB_ReceiveDataBuffer);
  
  for i := 0 to 3 do
    GreenLed := true;
    MDelay(500);
    GreenLed := false;
    MDelay(500);
  endfor;

  EnableInts;

  loop
    if SysTickHappened then
      SysTickHappened := False;
      MotorDoSysTick();
    endif;
    EssyBusDoRunMethods();
  endloop;
  
end EssyBusStepper.

