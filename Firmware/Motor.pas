//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

Unit Motor;

interface
// global part

{ $W+}                  // enable/disable warnings for this unit

uses ;

{--------------------------------------------------------------}
{ Const Declarations }
const
  SYSTICKTIME: Float = 0.0032768; //Systick time in seconds (20 Mhz div 32 (counter source) div 512 (triangle) div 4 (systick divider))
  SYSTICKSPERSECOND: Float = 1.0 / SYSTICKTIME;
  MINSPEED: Word = 5;
{$IDATA}
{--------------------------------------------------------------}
{ Type Declarations }
type
  TRampStatus = (rsOff, rsTrajectoryRampUp, rsTrajectoryRampDown, rsTrajectoryRampDownDelta, rsTrajectoryOnSpeed, rsConstantVelocityRampUp, rsConstantVelocityRampDown, rsConstantVelocityOnSpeed, rsStalled);

  TMotorDirection = (mdClockWise, mdCounterClockWise);
  
  TStopMode = (smHardStop, smDecelerate, smMoveTo, smStorePosition);

  TStopChannel = record
    Enabled: Boolean;
    StopAtHighLevel: Boolean;
    Active: Boolean;
    StopMode: TStopMode;
    Deceleration: LongInt;
    MoveDelta: LongInt;
  end;

{--------------------------------------------------------------}
{ Var Declarations }
var
  MaxSpeed, MoveCurrent, IdleCurrent: Word;
  RampUpAccel, RampDownAccel : Longint;
  DestinationPosition, StoredPosition: LongInt;
  MotorDirection: TMotorDirection;
  PrevPosition: LongInt;
  CurrentSpeed: Float;
  SystickCounter: Byte;
  RampUpAccelPerSystick, RampDownAccelPerSystick : Float;
  RampStatus: TRampStatus;
  StallDetectionEnabled: Boolean;
  StopChannel1, StopChannel2: TStopChannel;
  EarlyArrivalDelta: Word;
  
{--------------------------------------------------------------}
{ functions }
procedure MotorDoSysTick;
procedure MotorTrajectoryMove;
procedure MotorConstantVelocityMoveClockWise;
procedure MotorConstantVelocityMoveCounterClockWise;
procedure MotorConstantVelocityStop;
procedure InitMotor;

implementation
// local part

{--------------------------------------------------------------}
{ Type Declarations }
type
  PStopChannel = Pointer to TStopChannel;
{--------------------------------------------------------------}
{ Const Declarations }
const

{--------------------------------------------------------------}
{ Var Declarations }
{$IDATA}
var
  fTempPosition, fRampDownPosition: LongInt;
  fTempSpeedPerSystick: Float;
  fTempSystickWait: Byte;
  fTempStallDelta: LongInt;
  
{--------------------------------------------------------------}
{ functions }

procedure fStopMotor;
begin
  DisablePulseTimer();
  EarlyArrivalDelta := 0;
  SetCurrent(IdleCurrent);
  RedLed := false;
end;

procedure fStartMotor;
begin
  StopChannel1.Active := false;
  StopChannel2.Active := false;
  SetCurrent(MoveCurrent);
  RedLed := true;
  SetPulseTimerSpeed(MINSPEED);
  CurrentSpeed := MINSPEED;
  SystickCounter := 0;
  EnablePulseTimer();
end;

procedure fSignalMotorStopped;
begin
  if StopChannel1.Active then
    SetEssyBusInterruptReason(erStopchannel1Happened);
    return;
  endif;
  if StopChannel2.Active then
    SetEssyBusInterruptReason(erStopchannel2Happened);
    return;
  endif;
  SetEssyBusInterruptReason(erDestinationReached);
end;

function fCheckStopChannel(aStopChannelPin: Boolean; aStopChannel: PStopChannel): Boolean;
begin
  if aStopChannel^.Enabled then
    if not (aStopChannel^.StopAtHighLevel xor aStopChannelPin) then
      aStopChannel^.Enabled := false; //Only capture once
      case aStopChannel^.StopMode of
        smHardStop:
          fStopMotor();
          RampStatus := rsOff;
          aStopChannel^.Active := true;
          fSignalMotorStopped();
          return(true);|
        smDecelerate:
          RampStatus := rsConstantVelocityRampDown;
          aStopChannel^.Active := true;
          RampDownAccelPerSystick := Float(aStopChannel^.Deceleration) * SYSTICKTIME;
          return(true);|
        smMoveTo:
          RampStatus := rsTrajectoryRampDown;
          aStopChannel^.Active := true;
          if MotorDirection = mdClockWise then
            DestinationPosition := fTempPosition + aStopChannel^.MoveDelta;
          else
            DestinationPosition := fTempPosition - aStopChannel^.MoveDelta;
          endif;
          return(true);|
        smStorePosition:
          aStopChannel^.Active := false; //no need to set this to true as no action is needed
          StoredPosition := fTempPosition;
          return(false);|
      endcase;
    endif;
  endif;
  return(false);
end;

function fCheckStopChannels: Boolean;
begin
  if fCheckStopChannel(Stop1, @StopChannel1) then
    return(true);
  endif;
  if fCheckStopChannel(Stop2, @StopChannel2) then
    return(true);
  endif;
  return(false);
end;

procedure MotorDoSysTick;
var
  tempStatus: TRampStatus;
begin
  tempStatus := RampStatus;
  fTempPosition := GetPosition();
  if MotorDirection = mdClockWise then
    case tempStatus of
      rsOff:|
      rsTrajectoryRampUp:
        if fCheckStopChannels() then return; endif;
        if fTempPosition >= fRampDownPosition then
          RampStatus := rsTrajectoryRampDown;
          return;
        endif;
        CurrentSpeed := CurrentSpeed + RampUpAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed >= Float(MaxSpeed) then
          RampStatus := rsTrajectoryOnSpeed;
        endif;|
      rsTrajectoryRampDown:
        if fCheckStopChannels() then return; endif;
        if fTempPosition >= (DestinationPosition - LongInt(EarlyArrivalDelta)) then
          RampStatus := rsTrajectoryRampDownDelta;
          fSignalMotorStopped();
        endif;
        if fTempPosition >= DestinationPosition then
          fStopMotor();
          RampStatus := rsOff;
          return;
        endif;
        CurrentSpeed := CurrentSpeed - (((CurrentSpeed * CurrentSpeed)* SYSTICKTIME) / (4 * Float(DestinationPosition - fTempPosition)));
        SetPulseTimerSpeed(CurrentSpeed);|
      rsTrajectoryRampDownDelta:
        if fCheckStopChannels() then return; endif;
        if fTempPosition >= DestinationPosition then
          fStopMotor();
          RampStatus := rsOff;
          return;
        endif;
        CurrentSpeed := CurrentSpeed - (((CurrentSpeed * CurrentSpeed)* SYSTICKTIME) / (4 * Float(DestinationPosition - fTempPosition)));
        SetPulseTimerSpeed(CurrentSpeed);|
      rsTrajectoryOnSpeed:
        if fCheckStopChannels() then return; endif;
        if fTempPosition >= fRampDownPosition then
          RampStatus := rsTrajectoryRampDown;
        endif;|
      rsConstantVelocityRampUp:
        if fCheckStopChannels() then return; endif;
        CurrentSpeed := CurrentSpeed + RampUpAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed >= Float(MaxSpeed) then
          RampStatus := rsConstantVelocityOnSpeed;
        endif;|
        //if fCheckStopChannels() then return; endif;|
      rsConstantVelocityRampDown:
        CurrentSpeed := CurrentSpeed - RampDownAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed <= Float(MINSPEED) then
          fStopMotor();
          RampStatus := rsOff;
          fSignalMotorStopped();
          return;
        endif;|
      rsConstantVelocityOnSpeed:
        if fCheckStopChannels() then return; endif;|
      rsStalled:|
    endcase;
  else
    case tempStatus of
      rsOff: |
      rsTrajectoryRampUp:
        if fCheckStopChannels() then return; endif;
        if fTempPosition <= fRampDownPosition then
          RampStatus := rsTrajectoryRampDown;
          return;
        endif;
        CurrentSpeed := CurrentSpeed + RampUpAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed >= Float(MaxSpeed) then
          RampStatus := rsTrajectoryOnSpeed;
        endif;|
      rsTrajectoryRampDown:
        if fCheckStopChannels() then return; endif;
        if fTempPosition <= (DestinationPosition + LongInt(EarlyArrivalDelta)) then
          RampStatus := rsTrajectoryRampDownDelta;
          fSignalMotorStopped();
        endif;
        if fTempPosition <= DestinationPosition then
          fStopMotor();
          RampStatus := rsOff;
          return;
        endif;
        CurrentSpeed := CurrentSpeed - (((CurrentSpeed * CurrentSpeed)* SYSTICKTIME) / (4 * Float(fTempPosition - DestinationPosition)));
        SetPulseTimerSpeed(CurrentSpeed); |
      rsTrajectoryRampDownDelta:
        if fCheckStopChannels() then return; endif;
        if fTempPosition <= DestinationPosition then
          fStopMotor();
          RampStatus := rsOff;
          return;
        endif;
        CurrentSpeed := CurrentSpeed - (((CurrentSpeed * CurrentSpeed)* SYSTICKTIME) / (4 * Float(fTempPosition - DestinationPosition)));
        SetPulseTimerSpeed(CurrentSpeed); |
      rsTrajectoryOnSpeed:
        if fCheckStopChannels() then return; endif;
        if fTempPosition <= fRampDownPosition then
          RampStatus := rsTrajectoryRampDown;
        endif;|
      rsConstantVelocityRampUp:
        if fCheckStopChannels() then return; endif;
        CurrentSpeed := CurrentSpeed + RampUpAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed >= Float(MaxSpeed) then
          RampStatus := rsConstantVelocityOnSpeed;
        endif;|
        //if fCheckStopChannels() then return; endif;|
      rsConstantVelocityRampDown:
        CurrentSpeed := CurrentSpeed - RampDownAccelPerSystick;
        SetPulseTimerSpeed(CurrentSpeed);
        if CurrentSpeed <= Float(MINSPEED) then
          fStopMotor();
          RampStatus := rsOff;
          fSignalMotorStopped();
          return;
        endif;|
      rsConstantVelocityOnSpeed:
        if fCheckStopChannels() then return; endif;|
      rsStalled:|
    endcase;
  endif;
  if StallDetectionEnabled then
    if (tempStatus <> rsOff) and (tempStatus <> rsStalled) then
      inc(SystickCounter);
      if SystickCounter > 140 then
        SystickCounter := 0;
        if MotorDirection = mdClockWise then
          fTempStallDelta := fTempPosition - PrevPosition;
        else
          fTempStallDelta := PrevPosition - fTempPosition;
        endif;
        if fTempStallDelta < 3 then
          fStopMotor();
          RampStatus := rsStalled;
          SetEssyBusInterruptReason(erStalled);
          PrevPosition := fTempPosition;
          return;
        endif;
        PrevPosition := fTempPosition;
      endif;
    endif;
  endif;
end;

procedure MotorTrajectoryMove;
var
  numberOfRampUpPulses, numberOfRampDownPulses, minimumTrapezoidalPulses : Float;  //indien minder pulses -> driehoek
  tempPosition, trajectorySteps: LongInt;
begin
  tempPosition := GetPosition();
  if tempPosition = DestinationPosition then
    SetEssyBusInterruptReason(erDestinationReached);
    return;
  endif;
  numberOfRampUpPulses := (Float(MaxSpeed) * Float(MaxSpeed)) / Float(LongInt(2) * RampUpAccel);
  numberOfRampDownPulses := (Float(MaxSpeed) * Float(MaxSpeed)) / Float(LongInt(2) * RampDownAccel);
  minimumTrapezoidalPulses := numberOfRampUpPulses + numberOfRampDownPulses;
  if tempPosition < DestinationPosition then
    MotorDirection := mdClockWise;
    StepperDirection := False;
    trajectorySteps := DestinationPosition - tempPosition;
    if trajectorySteps < LongInt(EarlyArrivalDelta) then
      EarlyArrivalDelta := 0;
    endif;
    if  Float(trajectorySteps) >= minimumTrapezoidalPulses then  //trapezium
      fRampDownPosition := DestinationPosition - LongInt(Round(numberOfRampDownPulses));
    else   //driehoek
      fRampDownPosition := tempPosition  + LongInt(Round(Float(trajectorySteps) * (Float(RampDownAccel) / Float(RampUpAccel + RampDownAccel))));
    endif;
    PrevPosition := tempPosition - 1000; //for stall detection
  else
    MotorDirection := mdCounterClockWise;
    StepperDirection := True;
    trajectorySteps := tempPosition - DestinationPosition;
    if trajectorySteps < LongInt(EarlyArrivalDelta) then
      EarlyArrivalDelta := 0;
    endif;
    if  Float(trajectorySteps) >= minimumTrapezoidalPulses then  //trapezium
      fRampDownPosition := DestinationPosition + LongInt(Round(numberOfRampDownPulses));
    else   //driehoek
      fRampDownPosition := tempPosition  - LongInt(Round(Float(trajectorySteps) * (Float(RampDownAccel) / Float(RampUpAccel + RampDownAccel))));
    endif;
    PrevPosition := tempPosition + 1000; //for stall detection
  endif;

  RampUpAccelPerSystick := Float(RampUpAccel) * SYSTICKTIME;
  RampStatus := rsTrajectoryRampUp;
  fStartMotor();
end;

procedure fMotorConstantVelocityMove;
begin
  RampUpAccelPerSystick := Float(RampUpAccel) * SYSTICKTIME;
  RampStatus := rsConstantVelocityRampUp;
  fStartMotor();
end;

procedure MotorConstantVelocityMoveClockWise;
begin
  MotorDirection := mdClockWise;
  StepperDirection := False;
  PrevPosition := GetPosition() - 1000; //for stall detection
  fMotorConstantVelocityMove();
end;

procedure MotorConstantVelocityMoveCounterClockWise;
begin
  MotorDirection := mdCounterClockWise;
  StepperDirection := True;
  PrevPosition := GetPosition() + 1000; //for stall detection
  fMotorConstantVelocityMove();
end;

procedure MotorConstantVelocityStop;
begin
  RampDownAccelPerSystick := Float(RampDownAccel) * SYSTICKTIME;
  RampStatus := rsConstantVelocityRampDown;
end;

procedure InitMotor;
begin
  IdleCurrent := 1;
  StepperDisabled := true;
  SetCurrent(1);
  RedLed := false;
  RampStatus := rsOff;
  StallDetectionEnabled := false;
  PrevPosition := 0;
  fTempPosition := 0;
  SystickCounter := 0;
  StoredPosition := 0;
  EarlyArrivalDelta := 0;
end;

initialization
// at StartUp


end Motor.

