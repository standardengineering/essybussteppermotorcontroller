﻿namespace MotorTest;

interface

uses
  System.Windows.Forms,
  System.Drawing;

type
  MainForm = partial class
  {$REGION Windows Form Designer generated fields}
  private
    components: System.ComponentModel.Container := nil;
    btnStop: System.Windows.Forms.Button;
    btnStartCCW: System.Windows.Forms.Button;
    btnStartCW: System.Windows.Forms.Button;
    nudDestination: System.Windows.Forms.NumericUpDown;
    btnMoveTo: System.Windows.Forms.Button;
    btnClock: System.Windows.Forms.Button;
    label3: System.Windows.Forms.Label;
    label2: System.Windows.Forms.Label;
    label1: System.Windows.Forms.Label;
    nudDec: System.Windows.Forms.NumericUpDown;
    nudSpeed: System.Windows.Forms.NumericUpDown;
    nudAcc: System.Windows.Forms.NumericUpDown;
    btnEncoder: System.Windows.Forms.Button;
    button2: System.Windows.Forms.Button;
    button1: System.Windows.Forms.Button;
    method InitializeComponent;
  {$ENDREGION}
  end;

implementation

{$REGION Windows Form Designer generated code}
method MainForm.InitializeComponent;
begin
  var resources: System.ComponentModel.ComponentResourceManager := new System.ComponentModel.ComponentResourceManager(typeOf(MainForm));
  self.btnStartCW := new System.Windows.Forms.Button();
  self.btnStartCCW := new System.Windows.Forms.Button();
  self.btnStop := new System.Windows.Forms.Button();
  self.btnMoveTo := new System.Windows.Forms.Button();
  self.nudDestination := new System.Windows.Forms.NumericUpDown();
  self.btnClock := new System.Windows.Forms.Button();
  self.nudAcc := new System.Windows.Forms.NumericUpDown();
  self.nudSpeed := new System.Windows.Forms.NumericUpDown();
  self.nudDec := new System.Windows.Forms.NumericUpDown();
  self.label1 := new System.Windows.Forms.Label();
  self.label2 := new System.Windows.Forms.Label();
  self.label3 := new System.Windows.Forms.Label();
  self.btnEncoder := new System.Windows.Forms.Button();
  self.button1 := new System.Windows.Forms.Button();
  self.button2 := new System.Windows.Forms.Button();
  (self.nudDestination as System.ComponentModel.ISupportInitialize).BeginInit();
  (self.nudAcc as System.ComponentModel.ISupportInitialize).BeginInit();
  (self.nudSpeed as System.ComponentModel.ISupportInitialize).BeginInit();
  (self.nudDec as System.ComponentModel.ISupportInitialize).BeginInit();
  self.SuspendLayout();
  // 
  // btnStartCW
  // 
  self.btnStartCW.Enabled := false;
  self.btnStartCW.Location := new System.Drawing.Point(138, 201);
  self.btnStartCW.Name := 'btnStartCW';
  self.btnStartCW.Size := new System.Drawing.Size(128, 42);
  self.btnStartCW.TabIndex := 0;
  self.btnStartCW.Text := 'Start CW';
  self.btnStartCW.UseVisualStyleBackColor := true;
  self.btnStartCW.Click += new System.EventHandler(@self.btnStartCW_Click);
  // 
  // btnStartCCW
  // 
  self.btnStartCCW.Enabled := false;
  self.btnStartCCW.Location := new System.Drawing.Point(138, 249);
  self.btnStartCCW.Name := 'btnStartCCW';
  self.btnStartCCW.Size := new System.Drawing.Size(128, 42);
  self.btnStartCCW.TabIndex := 1;
  self.btnStartCCW.Text := 'Start CCW';
  self.btnStartCCW.UseVisualStyleBackColor := true;
  self.btnStartCCW.Click += new System.EventHandler(@self.btnStartCCW_Click);
  // 
  // btnStop
  // 
  self.btnStop.Enabled := false;
  self.btnStop.Location := new System.Drawing.Point(288, 225);
  self.btnStop.Name := 'btnStop';
  self.btnStop.Size := new System.Drawing.Size(128, 42);
  self.btnStop.TabIndex := 2;
  self.btnStop.Text := 'Stop';
  self.btnStop.UseVisualStyleBackColor := true;
  self.btnStop.Click += new System.EventHandler(@self.btnStop_Click);
  // 
  // btnMoveTo
  // 
  self.btnMoveTo.Enabled := false;
  self.btnMoveTo.Location := new System.Drawing.Point(138, 89);
  self.btnMoveTo.Name := 'btnMoveTo';
  self.btnMoveTo.Size := new System.Drawing.Size(128, 42);
  self.btnMoveTo.TabIndex := 3;
  self.btnMoveTo.Text := 'Move To :';
  self.btnMoveTo.UseVisualStyleBackColor := true;
  self.btnMoveTo.Click += new System.EventHandler(@self.btnMoveTo_Click);
  // 
  // nudDestination
  // 
  self.nudDestination.Location := new System.Drawing.Point(288, 102);
  self.nudDestination.Maximum := new System.Decimal(array of System.Int32([10000,
      0,
      0,
      0]));
  self.nudDestination.Minimum := new System.Decimal(array of System.Int32([10000,
      0,
      0,
      -2147483648]));
  self.nudDestination.Name := 'nudDestination';
  self.nudDestination.Size := new System.Drawing.Size(146, 20);
  self.nudDestination.TabIndex := 4;
  self.nudDestination.Value := new System.Decimal(array of System.Int32([10,
      0,
      0,
      0]));
  // 
  // btnClock
  // 
  self.btnClock.Enabled := false;
  self.btnClock.Location := new System.Drawing.Point(138, 370);
  self.btnClock.Name := 'btnClock';
  self.btnClock.Size := new System.Drawing.Size(128, 42);
  self.btnClock.TabIndex := 5;
  self.btnClock.Text := 'Clock';
  self.btnClock.UseVisualStyleBackColor := true;
  self.btnClock.Click += new System.EventHandler(@self.btnClock_Click);
  // 
  // nudAcc
  // 
  self.nudAcc.Location := new System.Drawing.Point(288, 12);
  self.nudAcc.Maximum := new System.Decimal(array of System.Int32([100000,
      0,
      0,
      0]));
  self.nudAcc.Minimum := new System.Decimal(array of System.Int32([10000,
      0,
      0,
      -2147483648]));
  self.nudAcc.Name := 'nudAcc';
  self.nudAcc.Size := new System.Drawing.Size(146, 20);
  self.nudAcc.TabIndex := 6;
  self.nudAcc.Value := new System.Decimal(array of System.Int32([10,
      0,
      0,
      0]));
  // 
  // nudSpeed
  // 
  self.nudSpeed.Location := new System.Drawing.Point(288, 38);
  self.nudSpeed.Maximum := new System.Decimal(array of System.Int32([27000,
      0,
      0,
      0]));
  self.nudSpeed.Minimum := new System.Decimal(array of System.Int32([10000,
      0,
      0,
      -2147483648]));
  self.nudSpeed.Name := 'nudSpeed';
  self.nudSpeed.Size := new System.Drawing.Size(146, 20);
  self.nudSpeed.TabIndex := 7;
  self.nudSpeed.Value := new System.Decimal(array of System.Int32([10,
      0,
      0,
      0]));
  // 
  // nudDec
  // 
  self.nudDec.Location := new System.Drawing.Point(288, 64);
  self.nudDec.Maximum := new System.Decimal(array of System.Int32([100000,
      0,
      0,
      0]));
  self.nudDec.Minimum := new System.Decimal(array of System.Int32([10000,
      0,
      0,
      -2147483648]));
  self.nudDec.Name := 'nudDec';
  self.nudDec.Size := new System.Drawing.Size(146, 20);
  self.nudDec.TabIndex := 8;
  self.nudDec.Value := new System.Decimal(array of System.Int32([10,
      0,
      0,
      0]));
  self.nudDec.ValueChanged += new System.EventHandler(@self.nudDec_ValueChanged);
  // 
  // label1
  // 
  self.label1.AutoSize := true;
  self.label1.Location := new System.Drawing.Point(247, 14);
  self.label1.Name := 'label1';
  self.label1.Size := new System.Drawing.Size(25, 13);
  self.label1.TabIndex := 9;
  self.label1.Text := 'acc';
  // 
  // label2
  // 
  self.label2.AutoSize := true;
  self.label2.Location := new System.Drawing.Point(247, 40);
  self.label2.Name := 'label2';
  self.label2.Size := new System.Drawing.Size(36, 13);
  self.label2.TabIndex := 10;
  self.label2.Text := 'speed';
  // 
  // label3
  // 
  self.label3.AutoSize := true;
  self.label3.Location := new System.Drawing.Point(247, 66);
  self.label3.Name := 'label3';
  self.label3.Size := new System.Drawing.Size(25, 13);
  self.label3.TabIndex := 11;
  self.label3.Text := 'dec';
  // 
  // btnEncoder
  // 
  self.btnEncoder.Enabled := false;
  self.btnEncoder.Location := new System.Drawing.Point(483, 225);
  self.btnEncoder.Name := 'btnEncoder';
  self.btnEncoder.Size := new System.Drawing.Size(128, 42);
  self.btnEncoder.TabIndex := 12;
  self.btnEncoder.Text := 'GetEncoder';
  self.btnEncoder.UseVisualStyleBackColor := true;
  self.btnEncoder.Click += new System.EventHandler(@self.btnEncoder_Click);
  // 
  // button1
  // 
  self.button1.Enabled := false;
  self.button1.Location := new System.Drawing.Point(575, 386);
  self.button1.Name := 'button1';
  self.button1.Size := new System.Drawing.Size(128, 42);
  self.button1.TabIndex := 13;
  self.button1.Text := 'Down To Fluid';
  self.button1.UseVisualStyleBackColor := true;
  self.button1.Click += new System.EventHandler(@self.button1_Click);
  // 
  // button2
  // 
  self.button2.Enabled := false;
  self.button2.Location := new System.Drawing.Point(575, 434);
  self.button2.Name := 'button2';
  self.button2.Size := new System.Drawing.Size(128, 42);
  self.button2.TabIndex := 14;
  self.button2.Text := 'Up To 1';
  self.button2.UseVisualStyleBackColor := true;
  self.button2.Click += new System.EventHandler(@self.button2_Click);
  // 
  // MainForm
  // 
  self.ClientSize := new System.Drawing.Size(769, 514);
  self.Controls.Add(self.button2);
  self.Controls.Add(self.button1);
  self.Controls.Add(self.btnEncoder);
  self.Controls.Add(self.label3);
  self.Controls.Add(self.label2);
  self.Controls.Add(self.label1);
  self.Controls.Add(self.nudDec);
  self.Controls.Add(self.nudSpeed);
  self.Controls.Add(self.nudAcc);
  self.Controls.Add(self.btnClock);
  self.Controls.Add(self.nudDestination);
  self.Controls.Add(self.btnMoveTo);
  self.Controls.Add(self.btnStop);
  self.Controls.Add(self.btnStartCCW);
  self.Controls.Add(self.btnStartCW);
  self.Icon := (resources.GetObject('$this.Icon') as System.Drawing.Icon);
  self.Name := 'MainForm';
  self.Text := 'MainForm';
  self.Load += new System.EventHandler(@self.MainForm_Load);
  self.FormClosing += new System.Windows.Forms.FormClosingEventHandler(@self.MainForm_FormClosing);
  (self.nudDestination as System.ComponentModel.ISupportInitialize).EndInit();
  (self.nudAcc as System.ComponentModel.ISupportInitialize).EndInit();
  (self.nudSpeed as System.ComponentModel.ISupportInitialize).EndInit();
  (self.nudDec as System.ComponentModel.ISupportInitialize).EndInit();
  self.ResumeLayout(false);
  self.PerformLayout();
end;
{$ENDREGION}

end.