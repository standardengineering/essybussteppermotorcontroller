﻿<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>MotorTest</RootNamespace>
    <OutputType>WinExe</OutputType>
    <AssemblyName>MotorTest</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <ApplicationIcon>Properties\App.ico</ApplicationIcon>
    <Configuration Condition="'$(Configuration)' == ''">Release</Configuration>
    <TargetFrameworkVersion>v3.5</TargetFrameworkVersion>
    <ProjectGuid>{1A9B49B4-CF3E-4213-AF48-77243A1A8912}</ProjectGuid>
    <RunPostBuildEvent>㏡</RunPostBuildEvent>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Debug' ">
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <OutputPath>.\bin\Debug</OutputPath>
    <GeneratePDB>True</GeneratePDB>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)' == 'Release' ">
    <OutputPath>.\bin\Release</OutputPath>
    <EnableAsserts>False</EnableAsserts>
  </PropertyGroup>
  <ItemGroup>
    <ProjectReference Include="..\..\..\Software\EssyBus\EssyBus.oxygene">
      <Project>{F45CE426-5D40-4378-8024-CC2A23B9D619}</Project>
      <HintPath>$(Project)\..\..\..\Software\EssyBus\bin\Debug\EssyBus.dll</HintPath>
      <Name>EssyBus</Name>
    </ProjectReference>
    <ProjectReference Include="..\EssyBusMotorController\EssyBusMotorController.oxygene">
      <Project>{48A034DA-D94C-4F91-A345-B4C43CA67E20}</Project>
      <HintPath>$(Project)\..\EssyBusMotorController\bin\Debug\EssyBusMotorController.dll</HintPath>
      <Name>EssyBusMotorController</Name>
    </ProjectReference>
    <ProjectReference Include="..\..\..\FTDIWrapper\FTDIWrapper\FTDIWrapper.chrome">
      <Project>{C7EC7049-E499-4E33-8F5A-6CA3AD4AE94A}</Project>
      <HintPath>$(Project)\..\..\..\FTDIWrapper\FTDIWrapper\bin\Debug\FTDIWrapper.dll</HintPath>
      <Name>FTDIWrapper</Name>
    </ProjectReference>
    <ProjectReference Include="..\..\..\Software Interfaces\IMotor\IMotor.oxygene">
      <Project>{A73A9BB7-FA0B-45E7-97F8-2820DC8371A4}</Project>
      <HintPath>$(Project)\..\..\..\Software Interfaces\IMotor\bin\Debug\IMotor.dll</HintPath>
      <Name>IMotor</Name>
    </ProjectReference>
    <Reference Include="mscorlib">
      <HintPath>mscorlib.dll</HintPath>
    </Reference>
    <ProjectReference Include="..\..\..\Software\Spindle\Spindle.oxygene">
      <Project>{4AC8D7DE-282D-4FAF-95D2-9AF3A5E97D0C}</Project>
      <HintPath>..\..\..\Software\Spindle\bin\Debug\Spindle.dll</HintPath>
      <Name>Spindle</Name>
    </ProjectReference>
    <Reference Include="System">
      <HintPath>System.dll</HintPath>
    </Reference>
    <Reference Include="System.Core">
      <HintPath>$(ProgramFiles)\Reference Assemblies\Microsoft\Framework\v3.5\System.Core.dll</HintPath>
    </Reference>
    <Reference Include="System.Data">
      <HintPath>System.Data.dll</HintPath>
    </Reference>
    <Reference Include="System.Data.DataSetExtensions">
      <HintPath>$(ProgramFiles)\Reference Assemblies\Microsoft\Framework\v3.5\System.Data.DataSetExtensions.dll</HintPath>
    </Reference>
    <Reference Include="System.Drawing">
      <HintPath>System.Drawing.dll</HintPath>
    </Reference>
    <Reference Include="System.Windows.Forms">
      <HintPath>System.Windows.Forms.dll</HintPath>
    </Reference>
    <Reference Include="System.Xml">
      <HintPath>System.Xml.dll</HintPath>
    </Reference>
    <Reference Include="System.Xml.Linq">
      <HintPath>$(ProgramFiles)\Reference Assemblies\Microsoft\Framework\v3.5\System.Xml.Linq.dll</HintPath>
    </Reference>
    <ProjectReference Include="..\..\..\Software\USB_EssyBus\USB_EssyBus.oxygene">
      <Project>{C808EBDF-D46E-4AD1-9A54-8D508C6F3A69}</Project>
      <HintPath>$(Project)\..\..\..\Software\USB_EssyBus\bin\Debug\USB_EssyBus.dll</HintPath>
      <Name>USB_EssyBus</Name>
    </ProjectReference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="Main.pas">
      <Subtype>Form</Subtype>
      <DesignableClassName>MotorTest.MainForm</DesignableClassName>
    </Compile>
    <Compile Include="Main.Designer.pas">
      <Subtype>Form</Subtype>
      <DesignableClassName>MotorTest.MainForm</DesignableClassName>
    </Compile>
    <EmbeddedResource Include="Main.resx" />
    <Compile Include="Program.pas" />
    <Content Include="Properties\App.ico" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Oxygene\RemObjects.Oxygene.targets" />
  <PropertyGroup>
    <PreBuildEvent />
  </PropertyGroup>
</Project>