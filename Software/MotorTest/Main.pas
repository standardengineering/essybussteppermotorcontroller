﻿namespace MotorTest;

interface

uses
  System.Drawing,
  System.Collections,
  System.Collections.Generic,
  System.Linq,
  System.Windows.Forms,
  System.ComponentModel,
  System.Threading,
  Essy.Motor,
  Essy.EssyBus,
  Essy.EssyBus.PhysicalLayer,
  Essy.EssyBusMotorController,
  Essy.Spindle,
  Essy.FTDIWrapper;

type
  /// <summary>
  /// Summary description for MainForm.
  /// </summary>
  MainForm = partial class(System.Windows.Forms.Form)
  private
    fMotor: IStoppableMotor;
    tempMotor : EssyBusMotorController;
    tempSpindle: Spindle;
    method MainForm_Load(sender: System.Object; e: System.EventArgs);
    method btnStartCW_Click(sender: System.Object; e: System.EventArgs);
    method btnStartCCW_Click(sender: System.Object; e: System.EventArgs);
    method btnStop_Click(sender: System.Object; e: System.EventArgs);
    method btnMoveTo_Click(sender: System.Object; e: System.EventArgs);
    method btnClock_Click(sender: System.Object; e: System.EventArgs);
    method nudDec_ValueChanged(sender: System.Object; e: System.EventArgs);
    method MainForm_FormClosing(sender: System.Object; e: System.Windows.Forms.FormClosingEventArgs);
    method btnEncoder_Click(sender: System.Object; e: System.EventArgs);
    method button1_Click(sender: System.Object; e: System.EventArgs);
    method button2_Click(sender: System.Object; e: System.EventArgs);
  protected
    method Dispose(disposing: Boolean); override;
  public
    constructor;
  end;

implementation

{$REGION Construction and Disposition}
constructor MainForm;
begin
  //
  // Required for Windows Form Designer support
  //
  InitializeComponent();

  //
  // TODO: Add any constructor code after InitializeComponent call
  //
end;

method MainForm.Dispose(disposing: Boolean);
begin
  if disposing then begin
    if assigned(components) then
      components.Dispose();

    //
    // TODO: Add custom disposition code here
    //
  end;
  inherited Dispose(disposing);
end;
{$ENDREGION}

method MainForm.MainForm_Load(sender: System.Object; e: System.EventArgs);
begin
  var ftdiList := FTDI_DeviceInfo.EnumerateDevices('ThunderBolt');
  if ftdiList.Count > 0 then
  begin
    var tempEBPHY := new USB_FTDI_RS485(ftdiList[0]);
    self.Text := tempEBPHY.GetInterruptLineStatus().ToString();
    var EB := new EssyBus(tempEBPHY);
    tempMotor := new EssyBusMotorController(3);
    EB.AddClient(tempMotor);
    self.fMotor := tempMotor;
    tempMotor.ResetDevice();
    Thread.Sleep(100);
    tempMotor.IdleCurrent := 20;
    tempMotor.MoveCurrent := 100;
    tempMotor.StallDetectionEnabled := True;
    tempMotor.Powered := True;
    tempSpindle := new Spindle(tempMotor, 25);
    tempSpindle.MoveCounterClockWiseUntilStopChannel(10, 10, 0);
    tempSpindle.WaitUntilStopCondition;
    tempSpindle.Position := 0;
    tempMotor.MoveCurrent := 250;

    var tempStopChannel := new SpindleStopChannel;
    tempStopChannel.MoveDelta := 0.5;
    tempStopChannel.StopMode := MotorStopChannelMode.MovoToDeltaPos;
    tempStopChannel.StopAtHighLevel := True;

    tempSpindle.SetSpindleStopChannel2(tempStopChannel);
    
    btnStartCCW.Enabled := True;
    btnStartCW.Enabled := True;
    btnStop.Enabled := True;
    btnMoveTo.Enabled := True;
    btnClock.Enabled := True;
    btnEncoder.Enabled := True;
    button1.Enabled := True;
    button2.Enabled := True;
  end;
end;

method MainForm.btnStartCW_Click(sender: System.Object; e: System.EventArgs);
begin
  fMotor.MoveClockWiseUntilStopChannel(100, 100, 0);
  self.Text := fMotor.WaitUntilStopCondition().ToString();
end;

method MainForm.btnStartCCW_Click(sender: System.Object; e: System.EventArgs);
begin
  fMotor.MoveCounterClockWiseUntilStopChannel(100, 100, 0);
  self.Text := fMotor.WaitUntilStopCondition().ToString();
end;

method MainForm.btnStop_Click(sender: System.Object; e: System.EventArgs);
begin
  fMotor.Stop(1000);
end;

method MainForm.btnMoveTo_Click(sender: System.Object; e: System.EventArgs);
begin
  btnMoveTo.Enabled := False;
  var dest := Convert.ToInt32(nudDestination.Value);
  var speed := Convert.ToInt32(nudSpeed.Value);
  var dec := Convert.ToInt32(nudDec.Value);
  var acc := Convert.ToInt32(nudAcc.Value);
  //fMotor.MoveToPosition(dest, acc, speed, dec);
  tempSpindle.MoveToPosition(dest, acc, speed, dec);
  btnMoveTo.Enabled := True;
end;

method MainForm.btnClock_Click(sender: System.Object; e: System.EventArgs);
begin
  for i: Int32 := 1 to 5 do
  begin
    fMotor.MoveToPosition(500, 1000, 1000, 1000);
    fMotor.MoveToPosition(0, 1000, 1000, 1000);
  end;
end;

method MainForm.nudDec_ValueChanged(sender: System.Object; e: System.EventArgs);
begin
  
end;

method MainForm.MainForm_FormClosing(sender: System.Object; e: System.Windows.Forms.FormClosingEventArgs);
begin
  fMotor.Powered := False;
end;

method MainForm.btnEncoder_Click(sender: System.Object; e: System.EventArgs);
begin
  self.Text := fMotor.Position.ToString();
end;

method MainForm.button1_Click(sender: System.Object; e: System.EventArgs);
begin
  tempSpindle.MoveClockWiseUntilStopChannel(10, 10, 2);
  self.Text := tempSpindle.WaitUntilStopCondition().ToString();
end;

method MainForm.button2_Click(sender: System.Object; e: System.EventArgs);
begin
  tempSpindle.StartMoveToPosition(1, 20, 20, 20);
  tempSpindle.WaitForMoveToPosition();
end;

end.