﻿//Copyright (C) 2008 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.EssyBusMotorController;

interface

uses
  Prism.StandardAspects,
  System.Threading,
  Essy.Motor,
  Essy.EssyBus;

type

  InterruptReason nested in EssyBusMotorController = public enum(none = 0, DestinationReached, StopChannel1Happened, StopChannel2Happened, Stalled) of Byte;

  StopChannelPullupState nested in EssyBusMotorController = public enum(NoPullUps = 0, ChannelOnePullUp, ChannelTwoPullUp, BothChannelsPullUp) of Byte;

  EssyBusMotorController = public class(EssyBusClient, IEssyBusClientInterrupt, IStoppableMotor)
  private
    fMovementCompleteWaitObject: EventWaitHandle := new EventWaitHandle(false, EventResetMode.AutoReset);
    fNumberOfStepsPerRevolution: System.UInt16 := 0;
    fWaitingForInterrupt: Boolean;
    fStallAsaStopChannel: Boolean := false;
    fLastIntReason: InterruptReason;
    fLastTargetPosition: Int32 := 0;
    fLastStoppedPosition: Int32 := 0;
    fTravelTime: TimeSpan;
    method set_StopChannelPullUps(value: StopChannelPullupState);
    method get_StoredPosition: Int32;
    method fStopChannelToByteArray(aStopChannel: MotorStopChannel): array of Byte;
    method set_Powered(value: Boolean);
    method set_StallDetectionEnabled(value: Boolean);
    [aspect: WriteCachedProperty]
    method fset_Speed(value: UInt16);
    [aspect: WriteCachedProperty]
    method fset_Deceleration(value: Int32);
    [aspect: WriteCachedProperty]
    method fset_Acceleration(value: Int32);
    method set_MoveCurrent(value: UInt16);
    method set_IdleCurrent(value: UInt16);
    method set_InterruptReason(value: InterruptReason);
    method get_InterruptReason: InterruptReason;
    method set_Position(value: System.Int32);
    method get_Position: System.Int32;
    method get_NumberOfStepsPerRevolution: System.UInt16;
    method fEnableHBridge;
    method fDisableHBridge;
    method fSetEarlyArrivalDelta(aValue: UInt16);
    method fCalculateTravelTime(aAcceleration, aSpeed, aDecelleration, aDistance: Double): TimeSpan;
  protected
  public
    method HandleInterrupt;
    method WaitForMoveToPosition;
    method Stop(aDeceleration: System.Int32);
    method StartMoveToPosition(aPosition: System.Int32; aAcceleration: System.Int32; aSpeed: System.UInt16; aDeceleration: System.Int32);
    method MoveToPosition(aPosition: System.Int32; aAcceleration: System.Int32; aSpeed: System.UInt16; aDeceleration: System.Int32);
    method MoveCounterClockWise(aAcceleration: System.Int32; aSpeed: System.UInt16);
    method MoveClockWise(aAcceleration: System.Int32; aSpeed: System.UInt16);
    method ResetDevice;
    method MoveClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: UInt16; params aStopChannels: array of Byte);
    method MoveCounterClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: Word; params aStopChannels: array of Byte);
    method WaitUntilStopCondition: Byte;
    property Position: System.Int32 read get_Position write set_Position;
    property NumberOfStepsPerRevolution: System.UInt16 read get_NumberOfStepsPerRevolution;
    property WaitingForInterrupt: System.Boolean read fWaitingForInterrupt;
    property IntReason: InterruptReason read get_InterruptReason write set_InterruptReason;
    [aspect: WriteCachedProperty]
    property IdleCurrent: UInt16 write set_IdleCurrent;
    [aspect: WriteCachedProperty]
    property MoveCurrent: UInt16 write set_MoveCurrent;
    [aspect: WriteCachedProperty]
    property StallDetectionEnabled: Boolean write set_StallDetectionEnabled;
    property Powered: Boolean write set_Powered;
    property StopChannelPullUps: StopChannelPullupState write set_StopChannelPullUps;
    property LastStoppedPosition: Int32 read fLastStoppedPosition;
    property EarlyArrivalDelta: UInt16 write fSetEarlyArrivalDelta;
    method SetMotorStopChannel1(aMotorStopChannel: MotorStopChannel);
    method SetMotorStopChannel2(aMotorStopChannel: MotorStopChannel);
    method MoveToPositionWithCapture(aPosition: Int32; aAcceleration: Int32; aSpeed: Word; aDeceleration: Int32; params aStopChannels: array of Byte): Int32;
    [aspect: Disposable]
    method Dispose;
  end;

implementation

method EssyBusMotorController.MoveClockWise(aAcceleration: System.Int32; aSpeed: System.UInt16);
begin
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.EncodeMethod(14);
end;

method EssyBusMotorController.MoveCounterClockWise(aAcceleration: System.Int32; aSpeed: System.UInt16);
begin
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.EncodeMethod(15);
end;

method EssyBusMotorController.MoveToPosition(aPosition: System.Int32; aAcceleration: System.Int32; aSpeed: System.UInt16; aDeceleration: System.Int32);
begin
  StartMoveToPosition(aPosition, aAcceleration, aSpeed, aDeceleration);
  WaitForMoveToPosition();
end;

method EssyBusMotorController.StartMoveToPosition(aPosition: System.Int32; aAcceleration: System.Int32; aSpeed: System.UInt16; aDeceleration: System.Int32);
begin
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.fset_Deceleration(aDeceleration);
  self.fMovementCompleteWaitObject:Reset();
  self.fWaitingForInterrupt := True;
  self.EncodeMethod(13, aPosition);
  var dist := Math.Abs(aPosition - fLastStoppedPosition);
  fTravelTime := fCalculateTravelTime(aAcceleration, aSpeed, aDeceleration, dist);
  self.fLastTargetPosition := aPosition;
end;

method EssyBusMotorController.Stop(aDeceleration: System.Int32);
begin
  self.fMovementCompleteWaitObject:Reset();
  self.fWaitingForInterrupt := True;
  self.fset_Deceleration(aDeceleration);
  self.EncodeMethod(16);
  if not assigned(fMovementCompleteWaitObject) then exit;
  self.fMovementCompleteWaitObject.WaitOne();
  self.fWaitingForInterrupt := False;
  if self.fLastIntReason <> InterruptReason.DestinationReached then
  begin
    raise new MotorException(String.Format('Motor: {2} with address #{1} has not reached destination({3}). Reason: {0}. Current Position: {4}. Start Position: {5}', self.fLastIntReason, self.Address, self.Name, self.fLastTargetPosition, self.Position, self.fLastStoppedPosition));
  end;
  fLastStoppedPosition := self.Position;
end;

method EssyBusMotorController.WaitForMoveToPosition;
begin
  if not assigned(fMovementCompleteWaitObject) then exit;
  if not self.fMovementCompleteWaitObject.WaitOne(fTravelTime) then
  begin
    raise new StalledMotorException(String.Format('Motor: {2} with address #{1} has not reached destination({3}). Reason: {0}. Current Position: {4}. Start Position: {5}', self.fLastIntReason, self.Address, self.Name, self.fLastTargetPosition, self.Position, self.fLastStoppedPosition));
  end;
  self.fWaitingForInterrupt := false;
  if self.fLastIntReason <> InterruptReason.DestinationReached then
  begin
    //retry one more time
    Thread.Sleep(2000);
    self.fMovementCompleteWaitObject:Reset();
    self.fWaitingForInterrupt := true;
    self.EncodeMethod(13, self.fLastTargetPosition);
    if not assigned(fMovementCompleteWaitObject) then exit;
    self.fMovementCompleteWaitObject.WaitOne();
    self.fWaitingForInterrupt := false;
    if self.fLastIntReason <> InterruptReason.DestinationReached then
    begin
      raise new MotorException(String.Format('Motor: {2} with address #{1} has not reached destination({3}). Reason: {0}. Current Position: {4}. Start Position: {5}', self.fLastIntReason, self.Address, self.Name, self.fLastTargetPosition, self.Position, self.fLastStoppedPosition));
    end;
  end;
  fLastStoppedPosition := self.Position;
end;

method EssyBusMotorController.get_NumberOfStepsPerRevolution: System.UInt16;
begin
  if self.fNumberOfStepsPerRevolution = 0 then
  begin
    self.EncodeFunction(10, out fNumberOfStepsPerRevolution);
  end;
  result := self.fNumberOfStepsPerRevolution;
end;

method EssyBusMotorController.HandleInterrupt;
begin
  var tempIntReason := self.IntReason;
  if tempIntReason <> InterruptReason.none then
  begin
    fLastIntReason := tempIntReason;
    self.IntReason := InterruptReason.none;    
    if not assigned(fMovementCompleteWaitObject) then exit;
    fMovementCompleteWaitObject.Set;
  end;
end;

method EssyBusMotorController.get_Position: System.Int32;
begin
  self.EncodeFunction(8, out result);
end;

method EssyBusMotorController.set_Position(value: System.Int32);
begin
  self.EncodeMethod(9, value);
  fLastStoppedPosition := value;
end;

method EssyBusMotorController.get_InterruptReason: InterruptReason;
begin
  var tempByte: Byte;
  self.EncodeFunction(1, out tempByte);
  result := tempByte as InterruptReason;
end;

method EssyBusMotorController.set_InterruptReason(value: InterruptReason);
begin
  self.EncodeMethod(2, Byte(value));
end;

method EssyBusMotorController.fEnableHBridge;
begin
  self.EncodeMethod(3);
end;

method EssyBusMotorController.fDisableHBridge;
begin
  self.EncodeMethod(4);
end;

method EssyBusMotorController.set_IdleCurrent(value: UInt16);
begin
  self.EncodeMethod(17, value);
end;

method EssyBusMotorController.set_MoveCurrent(value: UInt16);
begin
   self.EncodeMethod(18, value);
end;

method EssyBusMotorController.fset_Acceleration(value: Int32);
begin
  self.EncodeMethod(5, value);
end;

method EssyBusMotorController.fset_Deceleration(value: Int32);
begin
  self.EncodeMethod(6, value);
end;

method EssyBusMotorController.fset_Speed(value: UInt16);
begin
  self.EncodeMethod(7, value);
end;

method EssyBusMotorController.ResetDevice;
begin
  self.EncodeMethod(19);
end;

method EssyBusMotorController.MoveClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: UInt16; params aStopChannels: array of Byte);
begin
  fStallAsaStopChannel := False;
  var stop1 := False;
  var stop2 := False;
  if aStopChannels.Length > 3 then raise new MotorException('This motor only supports two stop channels + Stall');
  for each sc in aStopChannels do
  begin
    case sc of
      0:
      begin
        fStallAsaStopChannel := True;
        self.fWaitingForInterrupt := True;
      end;
      1:
      begin
        self.fWaitingForInterrupt := True;
        stop1 := True;
      end;
      2:
      begin
        self.fWaitingForInterrupt := True;
        stop2 := True;
      end;
    else
      raise new MotorException('This motor only supports two stop channels + Stall');
    end; // case
  end;
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.EncodeMethod(20, stop1, stop2);
end;

method EssyBusMotorController.MoveCounterClockWiseUntilStopChannel(aAcceleration: Int32; aSpeed: Word; params aStopChannels: array of Byte);
begin
  fStallAsaStopChannel := False;
  var stop1 := False;
  var stop2 := False;
  if aStopChannels.Length > 3 then raise new MotorException('This motor only supports two stop channels + Stall');
  for each sc in aStopChannels do
  begin
    case sc of
      0:
      begin
        fStallAsaStopChannel := True;
        self.fWaitingForInterrupt := True;
      end;
      1:
      begin
        self.fWaitingForInterrupt := True;
        stop1 := True;
      end;
      2:
      begin
        self.fWaitingForInterrupt := True;
        stop2 := True;
      end;
    else
      raise new MotorException('This motor only supports two stop channels + Stall');
    end; // case
  end;
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.EncodeMethod(21, stop1, stop2);
end;

method EssyBusMotorController.set_StallDetectionEnabled(value: Boolean);
begin
  self.EncodeMethod(11, value);
end;

method EssyBusMotorController.WaitUntilStopCondition: Byte;
begin
  result := 255;
  if not assigned(fMovementCompleteWaitObject) then exit;
  fMovementCompleteWaitObject.WaitOne();
  self.fWaitingForInterrupt := False;
  case self.fLastIntReason of
    InterruptReason.Stalled: result := 0;
    InterruptReason.StopChannel1Happened: result := 1;
    InterruptReason.StopChannel2Happened: result := 2;
  end; // case
end;

method EssyBusMotorController.set_Powered(value: Boolean);
begin
  if value then
    self.fEnableHBridge()
  else
    self.fDisableHBridge();
end;

method EssyBusMotorController.SetMotorStopChannel1(aMotorStopChannel: MotorStopChannel);
begin
  self.EncodeMethod(12, 1, fStopChannelToByteArray(aMotorStopChannel));
end;

method EssyBusMotorController.SetMotorStopChannel2(aMotorStopChannel: MotorStopChannel);
begin
  self.EncodeMethod(12, 2, fStopChannelToByteArray(aMotorStopChannel));
end;

method EssyBusMotorController.fStopChannelToByteArray(aStopChannel: MotorStopChannel): Array of Byte;
begin
  result := new Byte[12];
  result[0] := BoolToByte(false); //not a used field from this side
  result[1] := BoolToByte(aStopChannel.StopAtHighLevel);
  result[2] := BoolToByte(false); //not a used field from this side
  result[3] := Byte(aStopChannel.StopMode);
  BitConverter.GetBytes(aStopChannel.Deceleration).CopyTo(result, 4);
  BitConverter.GetBytes(aStopChannel.MoveDelta).CopyTo(result, 8);
end;

method EssyBusMotorController.set_StopChannelPullUps(value: StopChannelPullupState);
begin
  self.EncodeMethod(22, Byte(value));
end;

method EssyBusMotorController.get_StoredPosition: Int32;
begin
  if self.FirmwareVersion.Major < 9 then exit 0;
  self.EncodeFunction(23, out result);
end;

method EssyBusMotorController.MoveToPositionWithCapture(aPosition: Int32; aAcceleration: Int32; aSpeed: Word; aDeceleration: Int32; params aStopChannels: array of Byte): Int32;
begin
  var stop1 := false;
  var stop2 := false;
  if aStopChannels.Length > 2 then raise new MotorException('This motor only supports two stop channels');
  for each sc in aStopChannels do
  begin
    case sc of
      1:
      begin
        stop1 := true;
      end;
      2:
      begin
        stop2 := true;
      end;
    else
      raise new MotorException('This motor only supports two stop channels');
    end;
  end;
  self.fset_Acceleration(aAcceleration);
  self.fset_Speed(aSpeed);
  self.fMovementCompleteWaitObject:Reset();
  self.fWaitingForInterrupt := true;
  if self.FirmwareVersion.Major < 9 then
    self.EncodeMethod(13, aPosition)
  else
    self.EncodeMethod(24, aPosition, stop1, stop2);
  self.fLastTargetPosition := aPosition;
  WaitForMoveToPosition();
  result := get_StoredPosition();
end;

method EssyBusMotorController.fSetEarlyArrivalDelta(aValue: UInt16);
begin
  if self.FirmwareVersion.Major < 9 then exit;
  self.EncodeMethod(25, aValue);
end;

method EssyBusMotorController.Dispose;
begin
  if not assigned(fMovementCompleteWaitObject) then exit;
  fMovementCompleteWaitObject.Set();
  disposeAndNil(fMovementCompleteWaitObject);
end;

method EssyBusMotorController.fCalculateTravelTime(aAcceleration: Double; aSpeed: Double; aDecelleration: Double; aDistance: Double): TimeSpan;
begin
  var acc := Math.Max(aAcceleration, 1);
  var dec := Math.Max(aDecelleration, 1); 
  var s := Math.Max(aSpeed, 1);
  var s2 := s * s;
  var lacc := s2 / (2 * acc);
  var ldec := s2 / (2 * dec);
  var tacc := s / acc;
  var tdec := s / dec;
  var tTotal := tacc + tdec;
  if aDistance > (lacc + ldec) then //trapezoid  
  begin   
    var lSpeed := aDistance - (lacc + ldec);
    var tSpeed := lSpeed / s;
    tTotal := tTotal + tSpeed;
  end;
  exit TimeSpan.FromSeconds(tTotal + 3); //3 extra seconds 
end;


end.