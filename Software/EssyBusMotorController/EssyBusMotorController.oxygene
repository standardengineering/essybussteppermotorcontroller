﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>Essy.EssyBusMotorController</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>EssyBusMotorController</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Debug</Configuration>
    <Platform Condition="'$(Platform)' == ''">AnyCPU</Platform>
    <TargetFrameworkVersion>v4.8</TargetFrameworkVersion>
    <ProjectGuid>{48A034DA-D94C-4F91-A345-B4C43CA67E20}</ProjectGuid>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
    <ProjectView>ShowAllFiles</ProjectView>
    <Mode>Echoes</Mode>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Debug'">
    <OutputPath>.\bin\Debug\</OutputPath>
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>True</GenerateMDB>
    <Optimize>False</Optimize>
    <CpuType>anycpu</CpuType>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Release'">
    <OutputPath>.\bin\Release\</OutputPath>
    <EnableAsserts>False</EnableAsserts>
    <CpuType>anycpu</CpuType>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="mscorlib" />
    <Reference Include="System" />
    <ProjectReference Include="..\..\..\essybus\EssyBus.oxygene">
      <Name>EssyBus</Name>
      <Project>{f45ce426-5d40-4378-8024-cc2a23b9d619}</Project>
      <Private>True</Private>
      <HintPath>..\..\..\essybus\bin\Debug\EssyBus.dll</HintPath>
    </ProjectReference>
    <ProjectReference Include="..\..\..\oxygeneaspects\trunk\Source\Prism.StandardAspects\Prism.StandardAspects.oxygene">
      <Name>Prism.StandardAspects</Name>
      <Project>{fdfa9e51-2441-4785-9599-44a17d1b72c7}</Project>
      <Private>True</Private>
      <HintPath>..\..\..\oxygeneaspects\trunk\Source\Prism.StandardAspects\bin\Debug without initial Prime\Prism.StandardAspects.dll</HintPath>
    </ProjectReference>
    <ProjectReference Include="..\..\..\VariousLowLevelLibs\VariousLowLevelLibs.oxygene">
      <Name>VariousLowLevelLibs</Name>
      <Project>{6a05d4ed-184d-47cb-b24a-bec66d1c250c}</Project>
      <Private>True</Private>
      <HintPath>..\..\..\VariousLowLevelLibs\bin\Debug\VariousLowLevelLibs.dll</HintPath>
    </ProjectReference>
  </ItemGroup>
  <ItemGroup>
    <Compile Include="MotorController.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.Legacy.targets" />
</Project>